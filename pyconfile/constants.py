from typing import Dict

from typing_extensions import TypedDict

COMMENT_PREFIX: str = "# "
EC_CONFIG_FILENAME: str = ".editorconfig"
GITLAB_BASE_URL: str = "https://gitlab.com/joaommpalmeiro/pyconfile/-/blob"
MYPY_CONFIG_FILENAME: str = "mypy.ini"
RUFF_CONFIG_FILENAME: str = "ruff.toml"


# https://mypy.readthedocs.io/en/stable/typed_dict.html
# typing-extensions for TypedDict can be removed in Python 3.8+
# https://mypy.readthedocs.io/en/stable/typed_dict.html#class-based-syntax
class PackageNoTypes(TypedDict):
    package: str
    url: str
    metadata_url: str


PACKAGES_NO_TYPES: Dict[str, PackageNoTypes] = {
    "ConfigObj": {
        "package": "configobj",
        "url": "https://pypi.org/project/configobj/",
        "metadata_url": "https://github.com/DiffSK/configobj/blob/v5.0.8/setup.py",
    },
}
