from importlib.resources import open_text, read_text
from pathlib import Path
from typing import Iterable, List

from configobj import ConfigObj, Section
from tomlkit import document, dump, nl, parse, table
from tomlkit.items import (
    Array,
    Comment,
    Null,
    Table,
    Trivia,
    Whitespace,
    _ArrayItemGroup,
)

from . import __version__, templates
from .constants import (
    COMMENT_PREFIX,
    EC_CONFIG_FILENAME,
    GITLAB_BASE_URL,
    MYPY_CONFIG_FILENAME,
    PACKAGES_NO_TYPES,
    RUFF_CONFIG_FILENAME,
)

# Monkey patch:
# https://github.com/hukkin/tomli-w/blob/1.0.0/tomli_w/_writer.py#L14
# https://github.com/hukkin/tomli-w/blob/1.0.0/tomli_w/_writer.py#L136
# https://github.com/hukkin/tomli-w/blob/1.0.0/tomli_w/_writer.py#L29
# import tomli_w._writer
# tomli_w._writer.ARRAY_INDENT = " " * 2


# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/api.py#L285
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L328
def toml_comment(string: str) -> Comment:
    return Comment(Trivia(comment=string))


def get_gitlab_url(version_number: str, filename: str) -> str:
    version = f"v{version_number}"

    return f"{GITLAB_BASE_URL}/{version}/pyconfile/templates/{filename}"


def get_header(filename: str) -> str:
    origin_line = (
        f"{COMMENT_PREFIX}This file was generated with pyconfile {__version__}"
    )
    source_line = f"{COMMENT_PREFIX}Source: {get_gitlab_url(__version__, filename)}"

    return f"{origin_line}\n{source_line}"


# https://github.com/DiffSK/configobj/blob/v5.0.8/src/configobj/__init__.py#L1071
# https://github.com/DiffSK/configobj/blob/v5.0.8/src/configobj/__init__.py#L458
# https://bobbyhadz.com/blog/python-dictionary-replace-values#replace-values-in-a-dictionary-using-a-for-loop
# https://peps.python.org/pep-0008/
def remove_section_comments(section: Section, key: str) -> None:  # noqa: ARG001
    for member, comments in section.comments.items():
        if comments and comments[0] == "":  # noqa: PLC1901
            section.comments[member] = [""]
        else:
            section.comments[member] = []


def generate_ec_config() -> None:
    with open_text(templates, EC_CONFIG_FILENAME) as fp:
        ec_config = ConfigObj(fp, raise_errors=True)

    ec_config.initial_comment = [get_header(EC_CONFIG_FILENAME), ""]
    ec_config.walk(remove_section_comments)

    with Path(EC_CONFIG_FILENAME).open("wb") as fp:
        ec_config.write(fp)


# https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html#standard-duck-types
def get_ignore_missing_imports(
    package_names: Iterable[str],
) -> ConfigObj:
    config = ConfigObj()

    for package_name in package_names:
        metadata = PACKAGES_NO_TYPES[package_name]

        section_name = f"mypy-{metadata['package']}.*"
        options = {"ignore_missing_imports": True}
        comments = {
            "ignore_missing_imports": [
                f"Since there are no types available for the {package_name} package:",
                f"- {metadata['url']}",
                f"- {metadata['metadata_url']}",
            ],
        }

        config[section_name] = options
        config[section_name].comments = comments

    return config


# https://discuss.python.org/t/configparser-configparser-write-creates-2-empty-lines-at-the-end-of-my-file/14645/6
# https://bugs.python.org/issue32917
# https://stackoverflow.com/questions/18857352/remove-very-last-character-in-file
# https://configobj.readthedocs.io/en/latest/configobj.html#sections
# https://github.com/DiffSK/configobj/blob/v5.0.8/src/configobj/__init__.py#L458
# https://github.com/DiffSK/configobj/blob/v5.0.8/src/configobj/__init__.py#L1071
def generate_mypy_config() -> None:
    # mypy_config = ConfigParser(
    #     delimiters=("=",),
    #     comment_prefixes=("#",),
    #     empty_lines_in_values=False,
    # )

    # with open_text(templates, MYPY_CONFIG_FILENAME) as fp:
    #     mypy_config.read_file(fp)

    # with Path(MYPY_CONFIG_FILENAME).open("w") as fp:
    #     fp.write(f"{get_header(MYPY_CONFIG_FILENAME)}\n\n")
    #     mypy_config.write(fp, space_around_delimiters=True)
    with open_text(templates, MYPY_CONFIG_FILENAME) as fp:
        mypy_config = ConfigObj(fp, raise_errors=True)

    mypy_config.initial_comment = [get_header(MYPY_CONFIG_FILENAME), ""]
    mypy_config.walk(remove_section_comments)

    # TODO: Get input from CLI input and skip if empty
    extra_config = get_ignore_missing_imports(["ConfigObj"])

    with Path(MYPY_CONFIG_FILENAME).open("wb") as fp:
        mypy_config.write(fp)
        fp.write(mypy_config.newlines.encode())
        extra_config.write(fp)


def process_toml_table(item: Table) -> Table:
    clean_table = table(is_super_table=item.is_super_table())

    for table_item in item.value.body:
        # print(table_item[1], type(table_item[1]))
        if not isinstance(table_item[1], (Comment, Whitespace)):
            if isinstance(table_item[1], Table):
                clean_subtable = process_toml_table(table_item[1])
                clean_table.append(table_item[0], clean_subtable)
            else:
                clean_table.append(*table_item)

    return clean_table


# https://github.com/sdispater/tomlkit/blob/0.11.7/docs/quickstart.rst
# https://github.com/python/cpython/blob/3.7/Lib/importlib/resources.py
# https://github.com/python/cpython/blob/3.7/Lib/importlib/resources.py#L31
# https://github.com/python/cpython/blob/3.7/Lib/importlib/resources.py#L116
# https://github.com/python/cpython/blob/3.7/Lib/importlib/resources.py#L158
# https://tomlkit.readthedocs.io/en/latest/api/
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/container.py#L32
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/container.py#L475
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L487
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L328
# https://tomlkit.readthedocs.io/en/latest/api/#tomlkit.items.Item
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/container.py#L46
# `def body(self) -> List[Tuple[Optional[Key], Item]]:`
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L1122
# https://stackoverflow.com/questions/45360480/is-there-a-formatted-byte-string-literal-in-python-3-6
# https://github.com/hukkin/tomli-w/pull/27
# https://toml.io/en/v1.0.0#array
# https://tomlkit.readthedocs.io/en/latest/quickstart/#writing
# https://stackoverflow.com/a/33311330
# https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/container.py#L619
def generate_ruff_config() -> None:
    content = read_text(templates, RUFF_CONFIG_FILENAME)
    ruff_config = parse(content)

    # https://github.com/sdispater/tomlkit/issues/43 + https://github.com/sdispater/tomlkit/pull/187
    # print(ruff_config.unwrap())

    doc = document()
    doc.add(toml_comment(get_header(RUFF_CONFIG_FILENAME)))
    doc.add(nl())

    # https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L1206
    # https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L599
    # https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/items.py#L1088
    # https://github.com/sdispater/tomlkit/blob/0.11.7/tomlkit/container.py#L513
    for item in ruff_config.body:
        if not isinstance(item[1], (Comment, Whitespace)):
            if isinstance(item[1], Array):
                clean_array: List[_ArrayItemGroup] = []

                for value in item[1]._value:  # noqa: SLF001
                    if not isinstance(value.value, Null):
                        clean_array.append(value)

                item[1]._value = clean_array  # noqa: SLF001
                doc.append(*item)
            elif isinstance(item[1], Table):
                clean_table = process_toml_table(item[1])
                doc.append(item[0], clean_table)
            else:
                doc.append(*item)

    with Path(RUFF_CONFIG_FILENAME).open("w") as fp:
        dump(doc, fp, sort_keys=False)

    # import tomli
    # import tomli_w
    # ruff_config = tomli.loads(content)

    # with Path(RUFF_CONFIG_FILENAME).open("wb") as fp:
    #     fp.write(f"{get_header(RUFF_CONFIG_FILENAME)}\n\n".encode())
    #     tomli_w.dump(ruff_config, fp, multiline_strings=False)
