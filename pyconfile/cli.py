import click

from . import __version__
from .utils import generate_ec_config, generate_mypy_config, generate_ruff_config


# https://docs.python.org/3.7/library/configparser.html
# https://docs.python.org/3.7/library/configparser.html#customizing-parser-behaviour
# https://stackoverflow.com/a/20885799 + https://stackoverflow.com/a/60687710
# https://docs.python.org/3.7/library/importlib.html#module-importlib.resources
# https://docs.python.org/3.7/library/configparser.html#configparser-objects
# https://docs.python.org/3.7/library/configparser.html#customizing-parser-behaviour
# https://bugs.python.org/issue22253 + https://github.com/python/cpython/pull/2735
# https://editorconfig-python-core.readthedocs.io/en/latest/usage.html
# https://github.com/editorconfig/editorconfig-core-py
# https://github.com/editorconfig/editorconfig-core-py/blob/v0.12.3/editorconfig/ini.py
# https://github.com/candlepin/python-iniparse
# https://github.com/abravalheri/ini2toml
# https://github.com/catdad/editorconfig-parser
# https://github.com/adamchristiansen/dotfiles/blob/0dc194e5fb53418bfa8d18e09b964d52fca0baee/home/dot_local/bin/executable_restyle#L27
# https://docs.python.org/3.7/library/importlib.html#importlib.resources.read_text (str)
# https://docs.python.org/3.7/library/importlib.html#importlib.resources.open_text (fp)
# https://configobj.readthedocs.io/en/latest/configobj.html#introduction
# https://github.com/DiffSK/configobj
# https://github.com/matplotlib/matplotlib/blob/main/doc/api/prev_api_changes/api_changes_0.99.x.rst
# https://github.com/editorconfig-checker/editorconfig-checker + https://github.com/editorconfig-checker/editorconfig-checker.python
# https://configobj.readthedocs.io/en/latest/configobj.html#comments
# https://configobj.readthedocs.io/en/latest/configobj.html#walking-a-section
# https://configobj.readthedocs.io/en/latest/configobj.html#section-attributes
# https://configobj.readthedocs.io/en/latest/configobj.html#writing-a-config-file
# https://stackoverflow.com/a/37623873
# https://github.com/tiangolo/typer/issues/534
# https://click.palletsprojects.com/en/8.1.x/options/#choice-options
@click.command(context_settings={"max_content_width": 88})
@click.version_option(__version__)
def cli() -> None:
    """Bootstrap Python-related configuration files."""
    generate_ec_config()
    generate_mypy_config()
    generate_ruff_config()

    click.echo("Done!")
