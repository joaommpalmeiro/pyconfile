# Notes

- https://github.com/takumisoft68/vscode-markdown-table
- TOML:
  - https://toml.io/en/
  - https://docs.python.org/3/library/tomllib.html
  - https://github.com/hukkin/tomli-w + https://github.com/hukkin/tomli
  - https://github.com/sdispater/tomlkit
  - https://github.com/bobfang1992/pytomlpp
  - https://github.com/samuelcolvin/rtoml
  - https://github.com/pappasam/toml-sort
  - https://github.com/alethiophile/qtoml
- https://github.com/ssato/python-anyconfig
- https://github.com/GrahamDumpleton/wrapt
- https://github.com/christophercrouzet/gorilla
- https://github.com/dynaconf/dynaconf
- "ConfigObj is a simple but powerful config file reader and writer: an _ini file round tripper_." ([source](https://configobj.readthedocs.io/en/latest/configobj.html#introduction))
- Type stubs for third-party packages available in [typeshed](https://github.com/python/typeshed): https://github.com/typeshed-internal/stub_uploader/blob/main/data/uploaded_packages.txt

## Commands

```bash
hatch env prune
```

```bash
rm -rf .editorconfig mypy.ini
```

```bash
hatch -e dev run mypy --help
```

```bash
hatch -e dev run ruff --version
```

```bash
hatch -e dev run ruff check formatter.py
```

---

If you get the error below in WSL, run the following command in Windows PowerShell: `wsl --update` ([source](https://learn.microsoft.com/en-us/answers/questions/1194533/how-to-resolve-wsl-service-createinstance-0x800403))

```text
Error: 0x80040326
Error code: Wsl/Service/0x80040326
Press any key to continue...
```
