# pyconfile

Bootstrap Python-related configuration files.

## Supported tools

- [EditorConfig](https://editorconfig.org/) (via [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) 0.16.4)
- [mypy](https://www.mypy-lang.org/) 1.1.1
- [Ruff](https://beta.ruff.rs/) 0.0.259

## References

- https://github.com/editorconfig/editorconfig-vscode + https://github.com/editorconfig/editorconfig-vscode/blob/main/package.json
- https://www.npmjs.com/package/editorconfig + https://github.com/editorconfig/editorconfig-core-js
- https://editorconfig.org/ + https://spec.editorconfig.org/ + https://github.com/editorconfig/specification
- https://mypy.readthedocs.io/en/stable/config_file.html
- https://beta.ruff.rs/docs/configuration/ + https://beta.ruff.rs/docs/rules/

## Development

VS Code profile: [Python](https://github.com/joaopalmeiro/vscode-profiles/tree/main/Python)

```bash
hatch config set dirs.env.virtual .hatch
```

```bash
hatch config show --all
```

```bash
hatch env create dev
```

```bash
hatch env find
```

```bash
hatch env show
```

```bash
hatch version
```

```bash
hatch -e dev run pip show pyconfile
```

```bash
hatch -e dev run pyconfile --help
```

```bash
hatch -e dev run pyconfile
```

```bash
hatch run dev:check
```

Copy the output of the help command (`hatch -e dev run pyconfile --help`) and paste it in the [Usage](#usage) section.

## Deployment

```bash
hatch project metadata
```

```bash
hatch version
```

```bash
git tag
```

```bash
git ls-remote --tags origin
```

```bash
hatch version micro
```

or

```bash
hatch version minor
```

Commit the `pyconfile/__init__.py` file with the updated version.

```bash
git tag "v$(hatch version)"
```

```bash
git tag
```

```bash
git push origin --tags
```
